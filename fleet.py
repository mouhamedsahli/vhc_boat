# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
import time
import datetime
from openerp import tools
from openerp.osv.orm import except_orm
from openerp.tools.translate import _
from dateutil.relativedelta import relativedelta

class fleet_vehicle_tag(osv.Model):
    _name = 'fleet.vehicle.tag'
    _columns = {
        'name': fields.char('Name', required=True, translate=True),
    }

class fleet_vehicle_state(osv.Model):
    _name = 'fleet.vehicle.state'
    _order = 'sequence asc'
    _columns = {
        'name': fields.char('Name', required=True),
        'sequence': fields.integer('Sequence', help="Used to order the note stages")
    }
    _sql_constraints = [('fleet_state_name_unique','unique(name)', 'State name already exists')]


class fleet_vehicle_model(osv.Model):

    def _model_name_get_fnc(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):
            name = record.modelname
            if record.brand_id.name:
                name = record.brand_id.name + ' / ' + name
            res[record.id] = name
        return res

    def on_change_brand(self, cr, uid, ids, model_id, context=None):
        if not model_id:
            return {'value': {'image_medium': False}}
        brand = self.pool.get('fleet.vehicle.model.brand').browse(cr, uid, model_id, context=context)
        return {
            'value': {
                'image_medium': brand.image,
            }
        }

    _name = 'fleet.vehicle.model'
    _description = 'Model of a vehicle'
    _order = 'name asc'

    _columns = {
        'name': fields.function(_model_name_get_fnc, type="char", string='Name', store=True),
        'modelname': fields.char('Model name', size=32, required=True), 
        'model_boat':fields.boolean('Boat Model?'),
        'brand_id': fields.many2one('fleet.vehicle.model.brand', 'Model Brand', help='Brand of the vehicle'),
        'vendors': fields.many2many('res.partner', 'fleet_vehicle_model_vendors', 'model_id', 'partner_id', string='Responsable',),
        'image': fields.related('brand_id', 'image', type="binary", string="Logo"),
        'image_medium': fields.related('brand_id', 'image_medium', type="binary", string="Logo"),
        'image_small': fields.related('brand_id', 'image_small', type="binary", string="Logo"),        
        
  
    }
    
    _defaults={
               
               }


class fleet_vehicle_model_brand(osv.Model):
    _name = 'fleet.vehicle.model.brand'
    _description = 'Brand model of the vehicle'

    _order = 'name asc'

    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

    _columns = {
        'name': fields.char('Brand Name', size=64, required=True),
        'image': fields.binary("Logo",
            help="This field holds the image used as logo for the brand, limited to 1024x1024px."),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized photo", type="binary", multi="_get_image",
            store = {
                'fleet.vehicle.model.brand': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Medium-sized logo of the brand. It is automatically "\
                 "resized as a 128x128px image, with aspect ratio preserved. "\
                 "Use this field in form views or some kanban views."),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Smal-sized photo", type="binary", multi="_get_image",
            store = {
                'fleet.vehicle.model.brand': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Small-sized photo of the brand. It is automatically "\
                 "resized as a 64x64px image, with aspect ratio preserved. "\
                 "Use this field anywhere a small image is required."),
    }

fleet_vehicle_model_brand()



class fleet_vehicle(osv.Model):

    _inherit = 'mail.thread'

    def _vehicle_name_get_fnc(self, cr, uid, ids, prop, unknow_none, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):
            res[record.id] = record.model_id.brand_id.name or ''+ '/' + record.model_id.modelname + ' / ' + record.license_plate +' '+record.vin_sn +' '+record.driver_id.name or ''
        return res 
    def _get_default_state(self, cr, uid, context):
        try:
            model, model_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'fleet', 'vehicle_state_active')
        except ValueError:
            model_id = False
        return model_id

    _name = 'fleet.vehicle'
    _description = 'Information on a vehicle'
    _order= 'license_plate asc'
    _columns = {
        'name': fields.function(_vehicle_name_get_fnc, type="char", string='Name', store=True),
        'nom':fields.char('Nom'),
        #"Char" et "réglages"
        'genre':fields.char('Genre'),
        'chevalets':fields.char('Chevalets'),
        'char':fields.char('Char'),
        'reglages':fields.char('Réglages'),
        'company_id': fields.many2one('res.company', 'Company'),
        'license_plate': fields.char('Matricule', size=32, required=True, help='License plate number of the vehicle (ie: plate number for a car)'),
        'plaque':fields.char('Plaque') ,
        'usage_special':fields.char('Usage Spécial'),
        'code_usage_special': fields.char('Code sage special'),
        'vin_sn': fields.char('Chassis Number', size=32,required=True, help='Unique number written on the vehicle motor (VIN/SN number)'),
        'driver_id': fields.many2one('res.partner', 'Partenaire',required=False,domain="[('customer','=',True)]" ,help='Driver of the vehicle'),
        'genre_id': fields.many2one('genre.vehicule', 'Genre')  ,

        'odometer': fields.integer('odometer'),
        'odometer_unit': fields.selection([('kilometers', 'Kilometers'),('miles','Miles')], 'Odometer Unit', help='Unit of the odometer ',required=False),

        'model_id': fields.many2one('fleet.vehicle.model', 'Model', required=True, help='Model of the vehicle'),
        'acquisition_date': fields.date('Acquisition Date', required=False, help='Date when the vehicle has been bought'),
        'color': fields.char('Color', size=32, help='Color of the vehicle'),
        'state_id': fields.many2one('fleet.vehicle.state', 'State', help='Current state of the vehicle', ondelete="set null"),
        'seats': fields.integer('Seats Number', help='Number of seats of the vehicle'),
        'seats_avant': fields.integer('Seats Avant Number', help='Number of seats avant of the vehicle'),

        'doors': fields.integer('Doors Number', help='Number of doors of the vehicle'),
        'tag_ids' :fields.many2many('fleet.vehicle.tag', 'fleet_vehicle_vehicle_tag_rel', 'vehicle_tag_id','tag_id', 'Tags'),
        'transmission': fields.selection([('manual', 'Manual'), ('automatic', 'Automatic')], 'Transmission', help='Transmission Used by the vehicle'),
        'fuel_type': fields.selection([('gasoline', 'Gasoline'), ('diesel', 'Diesel'), ('electric', 'Electric'), ('hybrid', 'Hybrid')], 'Fuel Type', help='Fuel Used by the vehicle'),
        'horsepower': fields.integer('Horsepower'),
        'horsepower_tax': fields.float('Horsepower Taxation'),
        'power': fields.integer('Power (kW)', help='Power in kW of the vehicle'),
        'co2': fields.float('CO2 Emissions', help='CO2 emissions of the vehicle'),
        'image': fields.related('model_id', 'image', type="binary", string="Logo"),
        'image_medium': fields.related('model_id', 'image_medium', type="binary", string="Logo"),
        'image_small': fields.related('model_id', 'image_small', type="binary", string="Logo"),
        'car_value': fields.float('Car Value', help='Value of the bought vehicle'),
        
        'carrosserie_id':fields.many2one('carrosserie.carrosserie', 'Carrosserie')   ,
        'reception_par_type':fields.char('Reception par type') ,
        'cylindre':fields.integer('cylindre')   ,
        'puissance':fields.float('Puissance kw') ,
        'poids_a_vide':fields.float('Poids a vide')   ,
        'premiere_mise_circulation':fields.date('Première mise en circulation') ,
        'lieu_et_date': fields.char('Espace pour lieu et date')   ,
        'expertise': fields.char('Expertise')   ,
        'charge_utile':fields.float('Charge utile kg')   ,
        'poids_total':fields.float('Poids total kg')   ,
        'poids_ensemble':fields.float('Poids ensemble kg')   ,
        'poids_remarquable':fields.float('Poids remarquable kg')   ,
        'charge_sur_toit':fields.float('Charge sur le toit kg')   ,
        'code_emissions': fields.char('Code emissions kg')   ,
        }

    _defaults = {
        'doors': 5,
        'state_id': _get_default_state,
    }

    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        default.update({
            'log_fuel':[],
            'log_contracts':[],
            'log_services':[],
            'tag_ids':[],
            'vin_sn':'',
        })
        return super(fleet_vehicle, self).copy(cr, uid, id, default, context=context)

    def on_change_model(self, cr, uid, ids, model_id, context=None):
        if not model_id:
            return {}
        model = self.pool.get('fleet.vehicle.model').browse(cr, uid, model_id, context=context)
        return {
            'value': {
                'image_medium': model.image,
            }
        }

    def create(self, cr, uid, data, context=None):
        vehicle_id = super(fleet_vehicle, self).create(cr, uid, data, context=context)
        vehicle = self.browse(cr, uid, vehicle_id, context=context)
        self.message_post(cr, uid, [vehicle_id], body=_('Vehicle %s has been added to the fleet!') % (vehicle.license_plate), context=context)
        return vehicle_id

    def write(self, cr, uid, ids, vals, context=None):
        """
        This function write an entry in the openchatter whenever we change important information
        on the vehicle like the model, the drive, the state of the vehicle or its license plate
        """
        for vehicle in self.browse(cr, uid, ids, context):
            changes = []
            if 'model_id' in vals and vehicle.model_id.id != vals['model_id']:
                value = self.pool.get('fleet.vehicle.model').browse(cr,uid,vals['model_id'],context=context).name
                oldmodel = vehicle.model_id.name or _('None')
                changes.append(_("Model: from '%s' to '%s'") %(oldmodel, value))
            if 'driver_id' in vals and vehicle.driver_id.id != vals['driver_id']:
                value = self.pool.get('res.partner').browse(cr,uid,vals['driver_id'],context=context).name
                olddriver = (vehicle.driver_id.name) or _('None')
                changes.append(_("Driver: from '%s' to '%s'") %(olddriver, value))
            if 'state_id' in vals and vehicle.state_id.id != vals['state_id']:
                value = self.pool.get('fleet.vehicle.state').browse(cr,uid,vals['state_id'],context=context).name
                oldstate = vehicle.state_id.name or _('None')
                changes.append(_("State: from '%s' to '%s'") %(oldstate, value))
            if 'license_plate' in vals and vehicle.license_plate != vals['license_plate']:
                old_license_plate = vehicle.license_plate or _('None')
                changes.append(_("License Plate: from '%s' to '%s'") %(old_license_plate, vals['license_plate']))

            if len(changes) > 0:
                self.message_post(cr, uid, [vehicle.id], body=", ".join(changes), context=context)

        vehicle_id = super(fleet_vehicle,self).write(cr, uid, ids, vals, context)
        return True






class genre_vehicule(osv.osv):
    _name = 'genre.vehicule'
    _columns = {
        'name': fields.char('Genre'),
    }
    
class marque_type(osv.osv):
    _name = 'marque.type'
    _columns = {
        'name': fields.char('Marque et type'),
    }
class carrosserie_carrosserie(osv.osv):
    _name = 'carrosserie.carrosserie'
    _columns = {
        'name': fields.char('Carrosserie'),
    }
