# -*- coding: utf-8 -*-
from datetime import datetime
import pytz
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp.osv import fields, osv
from openerp.tools.translate import _
import time


class contrat(osv.osv):
    _inherit="account.analytic.account" 
    
    
#     def name_get(self, cr, uid, ids, context=None):
#         if isinstance(ids, (list, tuple)) and not len(ids):
#             return []
#         if isinstance(ids, (long, int)):
#             ids = [ids]
#         reads = self.read(cr, uid, ids, ['code','type_dossier_id','partner_id','name'], context=context)
#         res = []
#         
#         for record in reads:
#             name = ""
#             if record['code']:
#                 name = name +" "+record['code']
#             
#             if record['partner_id']:
#                 name = name +" "+record['partner_id'][1][:6]
#             if record['type_dossier_id']:
#                 # type_dossier = self.pool.get("heure.contract.category").browse(cr,uid,record['type_dossier_id'][0])
#                 name = name +" "+self.pool.get("heure.contract.category").browse(cr,uid,record['type_dossier_id'][0]).name
#             if record['name']:
#                 name =  name +" "+record['name']
#             res.append((record['id'], name))
#         return res
    
    

    
    
    

    def _get_complete_name(self, cr, uid, ids, name, arg, context={}):
        res = {}
        if isinstance(ids, (list, tuple)) and not len(ids):
            return []
        if isinstance(ids, (long, int)):
            ids = [ids]
        reads = self.read(cr, uid, ids, ['code','partner_id','name'], context=context)
              
        for record in reads:
#             name = ""
#             if record['code']:
#                 name = name +" "+record['code'] +":"
#              
#             if record['partner_id']:
#                 name = name +" "+ self.pool.get("res.partner").browse(cr,uid,record['partner_id'][0]).ref
#                 
#             if record['type_dossier_id']:
#                 # type_dossier = self.pool.get("heure.contract.category").browse(cr,uid,record['type_dossier_id'][0])
#                 name = name +" "+self.pool.get("heure.contract.category").browse(cr,uid,record['type_dossier_id'][0]).name
#             if record['nom']:
#                 name =  name +" "+record['nom']
            res[record['id']] = "test"
        return res
    def onchange_flotte(self, cr, uid, ids,fleet_id):
        fleet = self.pool.get('fleet.vehicle').browse(cr,uid,fleet_id)
        return {'value': {'partner_id': fleet.driver_id.id,'name':_('Ordre de travail: ') + fleet.name}}
                 
    
   
    _columns = {
    
    'nom':fields.char('Intervention',128,),
    'name':fields.function(_get_complete_name, method=True, type='char', required=False,string="Nom complet du dossier", store=True,help="Nom complet du dossier"),
    'fleet_id':fields.many2one('fleet.vehicle','Flotte'),
'account_line_ids':fields.one2many('account.analytic.line','account_id','Matériels et temps à facturer')

} 
    
    _defaults = {  
        'state': 'draft',
       
        
        }
contrat()

class account_line_sheet(osv.osv):
    _inherit = "account.analytic.line"    
    
    
  
    _columns = {
    
  
    
    'partner_id':fields.related('account_id', 'partner_id', 
            type='many2one', relation='res.partner', string='Client', store=True, readonly=True),              
}
    _defaults={
               'to_invoice':1 #lambda self,cr,uid,c:self.pool.get('ir.model.data').get_object_reference(cr, uid, 'hr_timesheet_invoice', 'timesheet_invoice_factor1').id
               }
account_line_sheet()

class time_sheet_sheet(osv.osv):
    _inherit = "hr.analytic.timesheet"   
    _order ="id desc"      
   
    _columns = {
  
    'categ_id':fields.many2one('product.category','Groupe'),
                 
} 
      
   
time_sheet_sheet()


 


class res_partner(osv.osv):

    _inherit ="res.partner"
    
    def onchange_partner(self, cr, uid, ids,name,ref):
      res = {}        
      if name and ref == False:           
                res = {'value': {'ref':name[:10],}}                 
      return res 
    

res_partner()    