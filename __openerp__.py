#  -*- coding: utf-8 -*-

{
    "name": "vehicule_bateau",
    "version": "0.1",
    "author": "fbl",
    "category": 'fleet_boat',
    'complexity': "easy",
    "description": """
    Gestion fleet/ Gestion boat
    """,
    'website': 'm',
    'images': [],
    'init_xml': [],
    "depends": ["sale", 'hr_timesheet','hr_timesheet_sheet',
    'hr_timesheet_invoice','hr_expense',
    'account_analytic_analysis','account' ],
    'data': [
     #   "security/fleet_security.xml",
        "views/fleet_cars.xml",
        "views/partner.xml",
        "views/fleet_data.xml",
       # "views/fleet_view.xml",
        "views/boat_view.xml",
        "views/contrat.xml",
        "views/hr_timesheet_invoice_view.xml"
        
    ],
    'demo_xml': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}
#  vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
