# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
import datetime
class motor_boat(osv.osv):    
    _name = "fleet.boat.moteur"
    _columns = {
        'boat_id':fields.many2one('fleet.vehicle', 'Bateau',domain=[('is_boat','=',True)]),       
        'marque_type_moteur':fields.char('Marque et type du moteur'),
        'num_moteur':  fields.char('N° du moteur'),
        'aprobation_de_type':fields.char('Aprobation de type'),
        'puissance':fields.float('Puissance kw'),    
        'bat_mot3_genre':fields.many2one('genre.moteur.bateau','Genre'),
        'horsepower': fields.integer('Horsepower'),
        'bat_mot3_annee':fields.char('Année de construction',size=4),
        'bat_mot3_cylindree': fields.integer('Nombre de cylindre'),
        'fuel_type': fields.selection([('gasoline', 'Gasoline'), ('diesel', 'Diesel'), ('electric', 'Electric'), ('hybrid', 'Hybrid')], 'Fuel Type', help='Fuel Used by the vehicle'),

    }

motor_boat()

class product_bateau(osv.osv):    
    _inherit = "fleet.vehicle"
    _columns = {
        'is_boat':fields.boolean('is boat?'),  
        'signe_distinct':  fields.char('Signes distinctifs'),  
        'date_import':fields.date('Date import'),
        'year_construction':fields.char('Année de construction',size=4),
        'bat_port': fields.char('Port'),
        'bat_num_place': fields.char('Numéro de place'),
        'bat_hivernage': fields.char('Hivernage'),
        'bat_hiv_place': fields.char('Numéro de place en Hiver'),
              
        'matiere_id':fields.many2one('matiere.matiere', 'Matière'),
        
        'charge':  fields.char('Charge',size=100),
        'carte_type':  fields.char('Carte Type',size=100),
        'motor_ids': fields.one2many('fleet.boat.moteur','boat_id','Marques et types moteurs'),
        'lieu_et_date': fields.char('Espace pour lieu et date'),
        'bat_notes':fields.text('Notes'),
        'bat_long_coque': fields.float('Longeur Coque') ,
        'bat_long_ht': fields.float('Longeur Ht') ,
        'bat_hauteur': fields.float('Hauteur') ,
        'bat_deplacement': fields.float('Deplacement') ,
        'bat_lest': fields.float('Lest') ,
        'bat_tirant': fields.float('Tirant') ,
        'bat_coeff': fields.float('Coeff') ,
       
        'longueur': fields.float('Longeur Flotte') ,
        'largeur': fields.float('Largeur Flotte') ,
        'surface':fields.float('Surface velique (m2)') ,
        
        'bat_coul_coque': fields.char('Couleur coque',size=100) ,
        'bat_coul_pont': fields.char('Couleur pont',size=100) ,
        'bat_carene': fields.char('Carene',size=100) ,
        
         'bat_inv1': fields.char('Inv1',size=100) ,
         'bat_inv1_num': fields.char('Inv1 Num',size=100) ,
         'bat_inv1_batt_1': fields.char('Inv1 batt1',size=100) ,
         
          'bat_inv2': fields.char('Inv2',size=100) ,
         'bat_inv2_num': fields.char('Inv2 Num',size=100) ,
         'bat_inv1_batt_2': fields.char('Inv2 batt1',size=100) ,
         
         'bat_inv2_batt_1': fields.char('Inv2 batt1',size=100) ,
         'bat_inv2_batt_1': fields.char('bat_inv2_batt_1',size=100) ,
         
    
        
        
        
 

    }

product_bateau()





class genre_bateau(osv.osv):
    _name = 'genre.bateau'
    _columns = {
        'name': fields.char('Genre bateau'),
    }
    
class genre_moteur_bateau(osv.osv):
    _name = 'genre.moteur.bateau'
    _columns = {
        'name': fields.char('Genre moteur bateau'),
}


class marque_type(osv.osv):
    _name = 'marque.type'
    _columns = {
        'name': fields.char('Marque et type'),
    }

class matiere_matiere(osv.osv):
    _name = 'matiere.matiere'
    _columns = {
        'name': fields.char('Matière'),
    }

class couleur_couleur(osv.osv):
    _name = 'couleur.couleur'
    _columns = {
        'name': fields.char('Couleur'),
    }


# longueur  largeur
class longueur_longueur(osv.osv):
    _name = 'longueur.longueur'
    _columns = {
        'name': fields.char('Longueur'),
    }

class largeur_largeur(osv.osv):
    _name = 'largeur.largeur'
    _columns = {
        'name': fields.char('Largeur'),
    }